package com.bherrys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_barang")
public class Barang implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column
	private String plu;

	@Column
	private String nama;

	@Column
	private String satuan;

	@Column
	private int stok;

	@Column
	private int stokmin;

	@Column
	private int hbeli;

	@Column
	private int hjual;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlu() {
		return plu;
	}

	public void setPlu(String plu) {
		this.plu = plu;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getSatuan() {
		return satuan;
	}

	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}

	public int getStok() {
		return stok;
	}

	public void setStok(int stok) {
		this.stok = stok;
	}

	public int getStokmin() {
		return stokmin;
	}

	public void setStokmin(int stokmin) {
		this.stokmin = stokmin;
	}

	public int getHbeli() {
		return hbeli;
	}

	public void setHbeli(int hbeli) {
		this.hbeli = hbeli;
	}
	
	public int getHjual() {
		return hjual;
	}

	public void setHjual(int hjual) {
		this.hjual = hjual;
	}


}