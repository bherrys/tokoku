package com.bherrys.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bherrys.dao.BarangDAO;
import com.bherrys.model.Barang;

@Service
@Transactional
public class BarangServiceImpl implements BarangService {

	@Autowired
	private BarangDAO barangDAO;

	@Override
	@Transactional
	public void addBarang(Barang barang) {
		barangDAO.addBarang(barang);
	}

	@Override
	@Transactional
	public List<Barang> getAllBarang() {
		return barangDAO.getAllBarang();
	}

	@Override
	@Transactional
	public void deleteBarang(Integer barangId) {
		barangDAO.deleteBarang(barangId);
	}

	public Barang getBarang(int brgid) {
		return barangDAO.getBarang(brgid);
	}

	public Barang updateBarang(Barang barang) {
		return barangDAO.updateBarang(barang);
	}

	public void setBarangDAO(BarangDAO barangDAO) {
		this.barangDAO = barangDAO;
	}

}
