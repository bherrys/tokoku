package com.bherrys.service;
import java.util.List;

import com.bherrys.model.Barang;

public interface BarangService {
	
	public void addBarang(Barang barang);

	public List<Barang> getAllBarang();

	public void deleteBarang(Integer barangId);

	public Barang getBarang(int barangid);

	public Barang updateBarang(Barang barang);
}
