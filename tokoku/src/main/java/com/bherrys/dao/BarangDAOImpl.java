package com.bherrys.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bherrys.model.Barang;

@Repository
public class BarangDAOImpl implements BarangDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addBarang(Barang barang) {
		sessionFactory.getCurrentSession().saveOrUpdate(barang);

	}

	@SuppressWarnings("unchecked")
	public List<Barang> getAllBarang() {

		return sessionFactory.getCurrentSession().createQuery("from Barang")
				.list();
	}

	@Override
	public void deleteBarang(Integer barangId) {
		Barang barang = (Barang) sessionFactory.getCurrentSession().load(
				Barang.class, barangId);
		if (null != barang) {
			this.sessionFactory.getCurrentSession().delete(barang);
		}

	}

	public Barang getBarang(int brgid) {
		return (Barang) sessionFactory.getCurrentSession().get(
				Barang.class, brgid);
	}

	@Override
	public Barang updateBarang(Barang barang) {
		sessionFactory.getCurrentSession().update(barang);
		return barang;
	}

}