package com.bherrys.dao;


import java.util.List;
import com.bherrys.model.Barang;

public interface BarangDAO {

	public void addBarang(Barang barang);

	public List<Barang> getAllBarang();

	public void deleteBarang(Integer barangId);

	public Barang updateBarang(Barang barang);

	public Barang getBarang(int barangid);
}
