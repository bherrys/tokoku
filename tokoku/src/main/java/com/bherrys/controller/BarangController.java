package com.bherrys.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.bherrys.model.Barang;
import com.bherrys.service.BarangService;

@Controller
public class BarangController {

	private static final Logger logger = Logger
			.getLogger(BarangController.class);

	public BarangController() {
		System.out.println("BarangController()");
	}

	@Autowired
	private BarangService barangService;

	@RequestMapping(value = "/")
	public ModelAndView listBarang(ModelAndView model) throws IOException {
		List<Barang> listBarang = barangService.getAllBarang();
		model.addObject("listBarang", listBarang);
		model.setViewName("home");
		return model;
	}

	@RequestMapping(value = "/newBarang", method = RequestMethod.GET)
	public ModelAndView newContact(ModelAndView model) {
		Barang barang = new Barang();
		model.addObject("barang", barang);
		model.setViewName("BarangForm");
		return model;
	}

	@RequestMapping(value = "/saveBarang", method = RequestMethod.POST)
	public ModelAndView saveBarang(@ModelAttribute Barang barang) {
		if (barang.getId() == 0) { // jika id barang 0 maka buat 
			// barang untuk update barang
			barangService.addBarang(barang);
		} else {
			barangService.updateBarang(barang);
		}
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/deleteBarang", method = RequestMethod.GET)
	public ModelAndView deleteBarang(HttpServletRequest request) {
		int barangId = Integer.parseInt(request.getParameter("id"));
		barangService.deleteBarang(barangId);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/editBarang", method = RequestMethod.GET)
	public ModelAndView editContact(HttpServletRequest request) {
		int barangId = Integer.parseInt(request.getParameter("id"));
		Barang barang = barangService.getBarang(barangId);
		ModelAndView model = new ModelAndView("BarangForm");
		model.addObject("barang", barang);

		return model;
	}

}