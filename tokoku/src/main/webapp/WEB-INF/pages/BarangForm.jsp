<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Data Baru/Ubah Data</title>
</head>
<body>
    <div align="center">
        <h1>Barang Baru/Edit Barang</h1>
        <form:form action="saveBarang" method="post" modelAttribute="barang">
        <table>
            <form:hidden path="id"/>
            <tr>
                <td>PLU :</td>
                <td><form:input path="plu" /></td>
            </tr>
            <tr>
                <td>Nama :</td>
                <td><form:input path="nama" /></td>
            </tr>
            <tr>
                <td>Satuan :</td>
                <td><form:input path="satuan" /></td>
            </tr>
            <tr>
                <td>Stok :</td>
                <td><form:input path="stok" /></td>
            </tr>
            <tr>
                <td>Stok Minimum :</td>
                <td><form:input path="stokmin" /></td>
            </tr>
            <tr>
                <td>Harga Beli :</td>
                <td><form:input path="hbeli" /></td>
            </tr>
            <tr>
                <td>Harga Jual :</td>
                <td><form:input path="hjual" /></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Save"></td>
            </tr>
        </table>
        </form:form>
    </div>
</body>
</html>