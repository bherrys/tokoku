<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pengelolaan Inventory Tokoku</title>
</head>
<body>
	<div align="center">
		<h1>Daftar Barang</h1>
		
		<table border="1">

			<th>PLU</th>
			<th>Nama Barang</th>
			<th>Satuan</th>
			<th>Stok</th>
			<th>Stok Minimum</th>
			<th>Harga Beli</th>
			<th>Harga Jual</th>

			<c:forEach var="barang" items="${listBarang}">
				<tr>

					<td>${barang.plu}</td>
					<td>${barang.nama}</td>
					<td>${barang.satuan}</td>
					<td>${barang.stok}</td>
					<td>${barang.stokmin}</td>
					<td>${barang.hbeli}</td>
					<td>${barang.hjual}</td>
					<td><a href="editBarang?id=${barang.id}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="deleteBarang?id=${barang.id}">Delete</a></td>

				</tr>
			</c:forEach>
		</table>
		<h4>
			Penambahan data barang baru di <a href="newBarang">sini</a>
		</h4>
	</div>
</body>
</html>